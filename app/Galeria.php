<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
    protected $table = 'galerias';
    protected $guarded = [];

    public function servicio()
    {
      return $this->belongsTo(Servicio::class);
    }
}
