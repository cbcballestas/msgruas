<nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="{{url('/')}}">Inicio</a></li>
          <li><a href="{{ url('nosotros') }}">Nuestra empresa</a></li>
          <li class="menu-has-children"><a>Servicios</a>
            <ul>
              @foreach($servicios_list as $servicio_n)
                <li><a href="{{ url('servicio/'.$servicio_n->slug) }}">{{ $servicio_n->titulo }}</a></li>
              @endforeach
            </ul>
          <li><a href="{{url('equipos')}}">Equipos</a></li>
          </li>
          <li><a href="{{url('contacto')}}">Contacto</a></li>
        </ul>
      </nav><!-- #nav-menu-containe-->