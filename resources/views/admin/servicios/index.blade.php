@extends('admin.template.template')
@section('title','Servicios')
@section('page_name','Servicios')
@section('small','Listado de servicios')
@section('breadcrumb')
    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Servicios</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">LISTADO TOTAL DE SERVICIOS</h3>
                    <!-- Button trigger modal -->
                    <a class="btn btn-primary btn-sm" href="{!! route('servicios.create') !!}">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        AGREGAR SERVICIO
                    </a>
                    <div class="box-tools">
                        {{ Form::open(['route' => 'servicios.index','method'=>'GET','autocomplete'=>'off']) }}
                        <div class="input-group input-group-sm form-group" style="width: 150px;">
                            <input type="text" name="servicio_search" class="form-control pull-right"
                                   placeholder="Buscar">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    @if(\Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible col-md-10" role="alert"
                             style="margin-left: 20px;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <strong>{{\Session::get('success_message')}}.</strong>
                        </div>
                    @endif
                    @if(\Session::has('delete_message'))
                        <div class="alert alert-danger alert-dismissible col-md-10" role="alert"
                             style="margin-left: 20px;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <strong>{{\Session::get('delete_message')}}.</strong>
                        </div>
                    @endif
                    <table class="table table-hover table-striped table-condensed">
                        <tr>
                            <th>Foto</th>
                            <th>Nombre_Servicio</th>
                            <th>Descripcion</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        @foreach ($servicios as $servicio)
                            <tr id="service-data">
                                <td><img src="{!! asset('storage/portadas/'.$servicio->foto) !!}"
                                         alt="{{ $servicio->name }}"
                                         class="img-circle img-responsive" width="200px"></td>
                                <td>{{ $servicio->titulo }}</td>
                                <td style="overflow:hidden;text-overflow: ellipsis;">{{ $servicio->descripcion }}</td>
                                @if ($servicio->estado == 1)
                                    <td><span class="label label-success">Activo</span></td>
                                @else
                                    <td><span class="label label-danger">Inactivo</span></td>
                                @endif
                                <td>
                                    <a href="{{route('servicios.edit',$servicio->slug)}}" class="btn btn-warning"
                                       title="Editar datos">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    {{ Form::open(['route' => ['servicios.destroy',$servicio->slug], 'method' => 'delete']) }}
                                    {{ Form::button('<i class="fa fa-trash"></i>',
                                    [
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger',
                                        'onclick' => 'return confirm("¿Ésta seguro de eliminar éste servicio?");'
                                    ] )  }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="text-center">
                    {{ $servicios->links() }}
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
@section('scripts')
    <!--<script src="{!! asset('modules/servicios/servicios.js') !!}"></script>-->
@endsection
