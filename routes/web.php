<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('nosotros', function () {
    return view('nosotros');
});

Route::get('equipos', function () {
    return view('equipos');
});

Route::get('servicio/{nombre}', 'ServiciosController@getServicio');

Route::get('contacto', function () {
    return view('contacto');
});

Route::get('nosotros', function () {
    return view('nosotros');
});


/*
/--------------------------------------------------------------------------
/ Rutas de autenticación
/--------------------------------------------------------------------------
*/

Route::get('admin/login','Auth\LoginController@showLoginForm');


/*
/--------------------------------------------------------------------------
/ Rutas administrador
/--------------------------------------------------------------------------
*/

Route::prefix('admin')->group(function () {

  Route::get('dashboard',function (){
    return view('admin.dashboard.dashboard');
  });
  Route::resource('servicios','ServiciosController');
  Route::resource('gruas','GruasController');
});
