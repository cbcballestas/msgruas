<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Servicio;

class ServiciosComposer
{
    public function compose(View $view)
    {
      $servicios_list = Servicio::all();
      $view->with('servicios_list', $servicios_list);
    }
}
