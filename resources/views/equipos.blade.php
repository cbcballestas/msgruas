@extends('templates.template')
@section('title','MS Machinery & Service | Equipos')
@section('content')
  <main id="main">
    <!--==========================
      Team Section
    ============================-->
    <div id="titulo" style="margin-top: 60px;"  class=" wow fadeInUp">
      <div style="background-image: url({{ asset('img/tittle.jpg') }}); height: 210px;"><br><br><br><br>
        <div class="text">
                <h1 style="color: orange; text-align: center"><b>Equipos</b></h1>
            </div>
      </div>
    </div>
    <section id="team">
      <div class="container wow fadeInUp">
        <div class="section-header">          
      </div>
      

      <div class="col-lg-3 col-md-6">
        <div class="member">
              <div class="pic"><img src="img/equipo1.jpg" alt="" ></div>
              <h4>GRUA TEREX T 560.</h4>
              <span>60 ton.</span>
              <div class="social">
               <!-- Button trigger modal -->
              <a class="btn btn-default" data-toggle="modal" data-target="#modal700"><i class="fa fa-info-circle"></i>Mas Informacion</a>
              <div id="modal700" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">GRUA TEREX T 560.</h4>
                    </div>
                    <div class="modal-body">
                      <!-- cuerpo del modal -->
                        <div>
                          <div class="box">
                            <div id="carousel-example-generic_70" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic_70" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic_70" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic_70" data-slide-to="2"></li>
                              <li data-target="#carousel-example-generic_70" data-slide-to="3"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <div class="item active" style="align-content: center;">
                                  <img src="{{ asset('img/Grua Terex T-560/1.jpg') }}" alt="" style="width: 400px; height: 300px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                  <img src="{{ asset('img/Grua Terex T-560/2.jpg') }}" alt="" style="width: 400px; height: 300px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                  <img src="{{ asset('img/Grua Terex T-560/3.jpg') }}" alt="" style="width: 400px; height: 300px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                  <img src="{{ asset('img/Grua Terex T-560/4.jpg') }}" alt="" style="width: 400px; height: 300px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <div>
                      <div class="box">
                        <div class="well">
                          <b>Descripcion:</b> <br>
                          <hr>
                          <p style="text-align: left;">
                          1.- Cap. 60t <br>
                          2.- Power Boom. 34m<br>
                          3.- Fixed Jib. 17,38m <br>
                          4.- Counterweight. 7.5t <br>
                          5.- Engine. Detroit  Diesel DD13 <br>
                          6.- Gross  Weight. 33t<br>
                          7.- Tires. F 425/65 R22.5 18PR. <br>
                          8.- Transmission. Eaton Fuller </p><br> 
                      <a href="#"><button class="btn btn-default">Descargar Manual</button></a>
                      </div>
                      </div>
                    </div>
                      <!-- fin modal -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            </div>
          </div>
        </div>
      </div>


      <div class="col-lg-3 col-md-6">
        <div class="member">
              <div class="pic"><img src="img/equipo2.png" alt=""></div>
              <h4>GRUA TEREX RT 450.</h4>
              <span>50 ton.</span>
              <div class="social">
               <!-- Button trigger modal -->
              <a class="btn btn-default" data-toggle="modal" data-target="#modal600"><i class="fa fa-info-circle"></i>Mas Informacion</a>
              <div id="modal600" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">GRUA TEREX RT 450.</h4>
                    </div>
                    <div class="modal-body">
                      <!-- cuerpo del modal -->
                        <div>
                          <div class="box">
                            <div id="carousel-example-generic_60" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic_60" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic_60" data-slide-to="1"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <div class="item active" style="align-content: center;">
                                  <img src="{{ asset('img/Grua TEREX RT 450/1.jpg') }}" alt="" style="width: 600px; height: 400px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                  <img src="{{ asset('img/Grua TEREX RT 450/2.jpg') }}" alt="" style="width: 600px; height: 400px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <div>
                      <div class="box">
                        <div class="well">
                          <b>Descripcion:</b> <br>
                          <hr>
                          <p style="text-align: left;">
                          1.- Cap. 50t<br>
                          2.- Power Boom. 32m<br>
                          3.- Fixed Jib. 17,68m <br>
                          4.- Counterweight. 6,35t <br>
                          5.- Engine. Cummins 6BTA 5.9 <br>
                          6.- Gross  Weight. 34,5t<br>
                          7.- Tires. 26,5x 25 26 PR.<br>
                          8.- Transmission. Full Power-Shift </p><br>
                      <a href="#"><button class="btn btn-default">Descargar Manual</button></a>
                      </div>
                      </div>
                    </div>
                      <!-- fin modal -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="member">
              <div class="pic"><img src="img/equipo3.jpg" alt="" style="width: 450px;height: 250px"></div>
              <h4>GRUA LIEBHERR LTM 1040/1.</h4>
              <span>40 ton.</span>
              <div class="social">
               <!-- Button trigger modal -->
              <a class="btn btn-default" data-toggle="modal" data-target="#modal500"><i class="fa fa-info-circle"></i>Mas Informacion</a>
              <div id="modal500" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">GRUA LIEBHERR LTM 1040/1.</h4>
                    </div>
                    <div class="modal-body">
                      <!-- cuerpo del modal -->
                        <div>
                          <div class="box">
                            <div id="carousel-example-generic_50" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic_50" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic_50" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic_50" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <div class="item active" style="align-content: center;">
                                 <img src="{{ asset('img/LIEBHERR LTM 1040, 40TM F-1/1.jpg') }}" alt="" style="width: 500px; height: 300px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                 <img src="{{ asset('img/LIEBHERR LTM 1040, 40TM F-1/2.jpg') }}" alt="" style="width: 500px; height: 300px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                 <img src="{{ asset('img/LIEBHERR LTM 1040, 40TM F-1/3.jpg') }}" alt="" style="width: 500px; height: 300px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <div>
                      <div class="box">
                        <div class="well">
                          <b>Descripcion:</b> <br>
                          <hr>
                          <p style="text-align: left;">
                          1.- Cap. 40TM ( 48 Us.T)<br>
                          2.- Power Boom. 30 m<br>
                          3.- Fixed Jib. 14,5 m <br>
                          4.- Counterweight. 7,5t <br>
                          5.- Engine.  LIEBHERR D 926. 6 CYL.<br>
                          6.- Gross Weight. 36t<br>
                          7.- Tires. 14.00.R25<br>
                          8.- Transmission. ZF Automatic.<br>
                          9.-  Safety Devices (LMI). LICCON </p><br>
                      <a href="#"><button class="btn btn-default">Descargar Manual</button></a>
                      </div>
                      </div>
                    </div>
                      <!-- fin modal -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="member">
              <div class="pic"><img src="img/equipo4.png" alt=""></div>
              <h4>GRUA TEREX RT 230</h4>
              <span>30 ton.</span>
              <div class="social">
               <!-- Button trigger modal -->
              <a class="btn btn-default" data-toggle="modal" data-target="#modal400"><i class="fa fa-info-circle"></i>Mas Informacion</a>
              <div id="modal400" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">GRUA TEREX RT 230</h4>
                    </div>
                    <div class="modal-body">
                      <!-- cuerpo del modal -->
                        <div>
                          <div class="box">
                            <div id="carousel-example-generic_40" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic_40" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic_40" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic_40" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <div class="item active" style="align-content: center;">
                                 <img src="{{ asset('img/Grua Terex RT 230/1.jpg') }}" alt="" style="width: 500px; height: 400px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                 <img src="{{ asset('img/Grua Terex RT 230/2.jpg') }}" alt="" style="width: 500px; height: 400px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                 <img src="{{ asset('img/Grua Terex RT 230/3.jpg') }}" alt="" style="width: 500px; height: 400px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <div>
                      <div class="box">
                        <div class="well">
                          <b>Descripcion:</b> <br>
                          <hr>
                          <p style="text-align: left;">
                          1.- Cap. 30t<br>
                          2.- Power Boom. 28m<br>
                          3.- Fixed Jib. 13m <br>
                          4.- Counterweight. 4t (8.900 LB) <br>
                          5.- Engine. Cummins 6BTA 5.9 <br>
                          6.- Gross  Weight. 25t<br>
                          7.- Tires. 26,5x 25 26 PR.<br>
                          8.- Transmission. Full Power-Shift. </p><br>
                      </div>
                      <a href="#"><button class="btn btn-default">Descargar Manual</button></a>
                      </div>
                    </div>
                      <!-- fin modal -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="member">
              <div class="pic"><img src="img/equipo5.png" alt=""></div>
              <h4>CAMION EFFER 220-4S</h4>
              <span>8.6 ton.</span>
              <div class="social">
               <!-- Button trigger modal -->
               <a class="btn btn-default" data-toggle="modal" data-target="#modal300"><i class="fa fa-info-circle"></i>Mas Informacion</a>

              <div id="modal300" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">EFFER 220 4S.</h4>
                    </div>
                    <div class="modal-body">
                      <!-- cuerpo del modal -->
                        <div>
                          <div class="box">
                            <div id="carousel-example-generic_30" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic_30" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic_30" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic_30" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <div class="item active" style="align-content: center;">
                                 <img src="{{ asset('img/Camion Effer 220-4S/1.jpg') }}" alt="" style="width: 600px; height: 400px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                 <img src="{{ asset('img/Camion Effer 220-4S/2.jpg') }}" alt="" style="width: 600px; height: 400px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                 <img src="{{ asset('img/Camion Effer 220-4S/3.jpg') }}" alt="" style="width: 600px; height: 400px">
                                <div class="carousel-caption">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <div>
                      <div class="box">
                        <div class="well">
                          <b>Descripcion:</b> <br>
                          <hr>
                          <p style="text-align: left;">
                          1.Cap. Max. 8.6t @ rmin = 2m<br>
                          2.Cap. Min. 1.2t @ rmax = 12.8m<br>
                          3.Alcance Max. 12.8 m<br>
                          4.Cap. Max Transp. 6.4t.<br>
                          5.Planchón de 6m.<br>
                          6.Seguros para contenedor<br>
                          7.Adaptador para Canasta de Fibra<br>
                          7.Engine, Cummins<br> 
                          </p><br>
                        <a href="#"><button class="btn btn-default">Descargar Manual</button></a>
                      </div>
                      </div>
                    </div>
                      <!-- fin modal -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="member">
              <div class="pic"><img src="img/equipo6.png" alt=""></div>
              <h4>PALFINGER 44002</h4>
              <span>14.2 ton.</span>
              <div class="social">
               <!-- Button trigger modal -->
               <a class="btn btn-default" data-toggle="modal" data-target="#modal200"><i class="fa fa-info-circle"></i>Mas Informacion</a>

              <div id="modal200" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">PALFINGER 44002.</h4>
                    </div>
                    <div class="modal-body">
                      <!-- cuerpo del modal -->
                        <div>
                          <div class="box">
                            <div id="carousel-example-generic_20" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic_20" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic_20" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic_20" data-slide-to="2"></li>
                              <li data-target="#carousel-example-generic_20" data-slide-to="3"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <div class="item active" style="align-content: center;">
                                <img src="{{ asset('img/PALFINGER 44002/1.jpg') }}" style="width: 600px;height: 300px;">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                <img src="{{ asset('img/PALFINGER 44002/2.jpg') }}" style="width: 600px;height: 300px;">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                <img src="{{ asset('img/PALFINGER 44002/3.jpg') }}" style="width: 600px;height: 300px;">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                <img src="{{ asset('img/PALFINGER 44002/4.jpg') }}" style="width: 600px;height: 300px;">
                                <div class="carousel-caption">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <div>
                      <div class="box">
                        <div class="well">
                          <b>Descripcion:</b> <br>
                          <hr>
                          <p style="text-align: left;">
                            1.- Cap. Max. 14.2t @ rmin = 2.2m<br>
                            2.-  Cap. Min. 700Kg @ rmax = 24,6m<br>
                            3.-  Alcance Max. 24.6 m<br>
                            4.-  Cap. Max Transp. 15t.<br>
                            5.-  Planchón de 6.5m.<br>
                            6.-  Seguros para contenedor<br>
                            7.-  Adaptador para Canasta de Fibra<br>
                            7.-  Engine, Iveco<br> 
                          </p><br>
                        <a href="#"><button class="btn btn-default">Descargar Manual</button></a>
                      </div>
                      </div>
                    </div>
                      <!-- fin modal -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="member">
              <div class="pic"><img src="img/equipo7.jpg" alt=""></div>
              <h4>TOYOTA 7FDU70.</h4>
              <span>7 ton.</span>
              <div class="social">
               <!-- Button trigger modal -->
               <a class="btn btn-default" data-toggle="modal" data-target="#modal100"><i class="fa fa-info-circle"></i>Mas Informacion</a>

              <div id="modal100" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">TOYOTA 7FDU70.</h4>
                    </div>
                    <div class="modal-body">
                      <!-- cuerpo del modal -->
                        <div>
                          <div class="box">
                            <div id="carousel-example-generic_10" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic_10" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic_10" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic_10" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <div class="item active" style="align-content: center;">
                                <img src="{{ asset('img/Montacargas Toyota Diesel/m1.jpg') }}" style="width: 600px;height: 300px;">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                <img src="{{ asset('img/Montacargas Toyota Diesel/m2.jpg') }}" style="width: 600px;height: 300px;">
                                <div class="carousel-caption">
                                </div>
                              </div>
                              <div class="item">
                                <img src="{{ asset('img/Montacargas Toyota Diesel/m3.jpg') }}" style="width: 600px;height: 300px;">
                                <div class="carousel-caption">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <div>
                      <div class="box">
                        <div class="well">
                          <b>Descripcion:</b> <br>
                          <hr>
                          <p style="text-align: left;">
                            1.Cap. Max. 8.6t @ rmin = 2m<br>
                            2.Cap. Min. 1.2t @ rmax = 12.8m<br>
                            3.Alcance Max. 12.8 m<br>
                            4.Cap. Max Transp. 6.4t.<br>
                            5.Planchón de 6m.<br>
                            6.Seguros para contenedor<br>
                            7.Adaptador para Canasta de Fibra<br>
                            7.Engine, Cummins 
                          </p><br>
                        <a href="#"><button class="btn btn-default">Descargar Manual</button></a>
                      </div>
                      </div>
                    </div>
                      <!-- fin modal -->
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
        <div class="box">
          <hr>
          <img src="{{ asset('img/marca.jpg') }}" style="width: 300px">
        </div>
      </div>

      <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
            <div class="box">
              <hr>
              <img src="{{ asset('img/marca1.jpg') }}" style="width: 300px">
            </div>
      </div>

      <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.8s">
            <div class="box">
              <hr>
              <img src="{{ asset('img/marca2.jpg') }}"  style="width: 300px" >
            </div>
      </div>
    </section><!-- #team -->
  </main>
  @endsection

  