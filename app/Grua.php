<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grua extends Model
{
    protected $table = 'gruas';
    protected $fillable = [
      'description',
      'capacity',
      'power_boom',
      'fixed_jib',
      'counterweight',
      'engine',
      'gross_weight',
      'tires',
      'transmission'
    ];
}
