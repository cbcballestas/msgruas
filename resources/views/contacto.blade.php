@extends('templates.template')
@section('title','MS Machinery & Service | Contacto')
@section('content')
<main id="main">
    <section id="contact">
      <div id="titulo" class=" wow fadeInUp">
        <div style="background-image: url(img/tittle.jpg); height: 210px;" class="img-rounded"><br><br><br><br>
          <div class="text">
                  <h1 class="section-title" style="color: orange; text-align: center"><b>CONTACTO MACHINERY & SERVICE Colombia</b></h1> 
              </div>
        </div>
      </div>
<br><br><br>

      <div id="google-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2202.1668074703384!2d-75.53684779159308!3d10.436221888119546!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8ef62f8e589ae87b%3A0x6b98948383d9b8f8!2sEDIFICIO+MARBELLA+REAL!5e0!3m2!1ses-419!2sco!4v1523391936117" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>

      <div class="container wow fadeInUp">
        <div class="row justify-content-center">

          <div class="col-lg-3 col-md-4">

            <div class="info">
              <div>
                <i class="fa fa-map-marker"></i>
                <p style="text-align: justify;"><b>Oficina Administrativa</b><br>EDIFICIO MARBELLA REAL, Cl. 47 #46-120A, oficina 12, cartagena de Indias</p>
              </div>

              <div>
                <i class="fa fa-map-marker"></i>
              </div>
                <p style="text-align: justify;"><b>Base de operaciones</b><br>Km 6. Variante Mamonal No. 7-19 Cartagena.</p>
              
              <div>
                <i class="fa fa-user-o"></i>
                <p><b>Ing. Gerzon Duran</b> <br>Director Comercial</p>
              </div>

              <div>
                <i class="fa fa-envelope"></i>
                <p>administracion@msgruas.com</p>
              </div>

              <div>
                <i class="fa fa-phone"></i>
                <p>(+57) 318 8372111 / (+57) 317 8335744</p>
              </div>
            </div>
          </div>

          <div class="col-lg-5 col-md-8">
            <div class="form">
              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>
              <form action="" method="post" role="form" class="contactForm">
                <div class="form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Correo electronico" data-rule="email" data-msg="El campo es obligatorio, ingrese un valor correcto" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Telefono" data-rule="Telefono" data-msg="Ingrese un valor correcto para este campo." />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Direccion" data-rule="direccion" data-msg="Ingrese un valor correcto para este campo." />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Ingrese un comentario." placeholder="Comentarios"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit">Enviar</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #contact -->

</main>
@endsection