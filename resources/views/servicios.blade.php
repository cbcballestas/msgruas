@extends('templates.template')
@section('content')
    <main id="main">
        <!--==========================
          Services Section
        ============================-->
        <div id="titulo" style="margin-top: 60px;" class=" wow fadeInUp">
            <div style="background-image: url({{ asset('img/tittle.jpg') }}); height: 210px;"><br><br><br><br>
                <div class="text">
                    <h1 style="color: orange; text-align: center"><b>{{ $servicio->titulo}}</b></h1><br>
                </div>
            </div>
        </div>

        <section id="about">
            <div class="container">
                <div class="row about-container">
                    <div class="col-lg-6 background order-lg-2 order-1 wow fadeInRight">
                        <img src="{!! asset('storage/portadas/'.$servicio->foto) !!}" style="width: 300px;height: 200px" class="img-responsive">
                    </div>
                    <div class="col-lg-6 content order-lg-1 order-2 ">
                        <p style="text-align: justify;">{{ $servicio->descripcion }}</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <!-- galeria-->
                <div class="container"><br>
                    <h1 style="text-align: center; color: orange;"><b>Nuestros Equipos En Categoria</b></h1>
                    <hr style="color: orange">

                    <div class="container">
                        <div class="row">
                            <div class='list-group gallery'>
                                @foreach( $servicio->galerias as $galeria )
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <a class="thumbnail fancybox" rel="ligthbox"
                                           href="{!! asset('storage/galerias/'.$servicio->slug.'/'.$galeria->img_name) !!}">
                                            <img class="img-responsive" alt="" src="{!! asset('storage/galerias/'.$servicio->slug.'/'.$galeria->img_name) !!}"/>
                                            <div class='text-right'>
                                                <small class='text-muted'>{{ $servicio->titulo }}</small>
                                            </div> <!-- text-right / end -->
                                        </a>
                                    </div> <!-- col-6 / end -->
                                @endforeach
                            </div> <!-- list-group / end -->
                        </div> <!-- row / end -->
                    </div> <!-- container / end -->


                </div>
                <!-- end galeria-->
            </div>
        </section><!-- #about -->
    </main>
<main id="main">
    <!--==========================
      Services Section
    ============================-->
     <div id="titulo" style="margin-top: 60px;"  class=" wow fadeInUp">
      <div style="background-image: url({{ asset('img/tittle.jpg') }}); height: 210px;"><br><br><br><br>
        <div class="text">
                <h1 style="color: orange; text-align: center"><b>{{ $servicio->titulo}}</b></h1><br>
            </div>
      </div>
    </div>

    <section id="about">
      <div class="container">
        <div class="row about-container">
          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInRight">
            <img src="{{ asset('img/equipo1.jpg') }}" style="width: 300px;height: 200px">
          </div>
          <div class="col-lg-6 content order-lg-1 order-2 ">
            <p style="text-align: justify;">{{ $servicio->descripcion }}</p>
          </div>
        </div>
      </div>
      <div class="container">
        <!-- galeria-->
          <div class="container"><br>
            <h1 style="text-align: center; color: orange;"><b>Nuestros Equipos En Categoria</b></h1>
            <hr style="color: orange">

<div class="container">
  <div class="row">
    <div class='list-group gallery'>
            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                    <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                    <div class='text-right'>
                        <small class='text-muted'>Image Title</small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
        </div> <!-- list-group / end -->
  </div> <!-- row / end -->
</div> <!-- container / end -->



          </div>
        <!-- end galeria-->
      </div>
    </section><!-- #about -->
  </main>
@endsection
