<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'servicios';

    public function galerias()
    {
        return $this->hasMany(Galeria::class);
    }

    public function scopeSearch($query, $s)
    {
        return $query->where('titulo','like','%'.$s.'%')
                     ->orderBy('created_at');
    }
}
