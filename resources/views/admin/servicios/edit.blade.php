@extends('admin.template.template')
@section('title','Servicios - Editar Servicio: '.$servicio->titulo)
@section('page_name','Servicios')
@section('small','Editar Servicio')
@section('breadcrumb')
    <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Editar Servicio</li>
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h2 class="box-title">EDITAR DATOS DEL SERVICIO : {{ucwords($servicio->titulo)}}</h2>
        </div>
        {!! Form::model($servicio,['route' => ['servicios.update',$servicio->slug],'role'=>'form','files'=>true,'autocomplete'=>'off','method'=>'PUT']) !!}
        <div class="box-body">
            <div class="row">
                <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('titulo', 'Nombre del servicio') !!}
                    {!! Form::text('titulo', null, ['class' => 'form-control', 'required' => 'required','autofocus']) !!}
                    <small class="text-danger">{{ $errors->first('titulo') }}</small>
                </div>
            </div>
            <div class="row">
                <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('descripcion', 'Descripción del servicio') !!}
                    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('descripcion') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('profile_photo') ? ' has-error' : '' }}">
                {!! Form::label('profile_photo', 'Portada de servicio') !!}
                {!! Form::file('profile_photo') !!}
                <p class="help-block">Si desea cambiar la portada del servicio, click en examinar</p>
                <small class="text-danger">{{ $errors->first('profile_photo') }}</small>
            </div>
            <div class="form-group">
                <label>Portada actual</label>
                <div class="form-group">
                    <img src="{!! asset('storage/portadas/'.$servicio->foto) !!}"
                         alt="{{ $servicio->name }}"
                         class="img-responsive" width="300px">
                </div>
            </div>
            <div class="form-group{{ $errors->has('galerias') ? ' has-error' : '' }}">
                {!! Form::label('galerias', 'Agregar fotos a la galería') !!}
                {!! Form::file('galerias[]') !!}
                <p class="help-block">Si desea agregar alguna foto para la galería.click en examinar.</p>
                <small class="text-danger">{{ $errors->first('galerias') }}</small>
            </div>
            <div class="form-group">
                <h1 class="text-center">Galería Actual</h1>
                <div class="container-fluid">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            @foreach( $servicio->galerias as $galeria )
                                <li data-target="#carousel-example-generic" data-slide-to="{{ $loop->index }}"
                                    class="{{ $loop->first ? 'active' : '' }}"></li>
                            @endforeach
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            @foreach( $servicio->galerias as $galeria )
                                <div class="item {{ $loop->first ? ' active' : '' }}">
                                    <img src="{!! asset('storage/galerias/'.$servicio->slug.'/'.$galeria->img_name) !!}"
                                         class="img-responsive" width="1000px">
                                </div>
                            @endforeach
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button"
                           data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button"
                           data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="btn-group pull-right" style="margin-right:20px">
                <div class="row">
                    {!! Form::submit("Actualizar Datos", ['class' => 'btn btn-success btn-flat btn-lg']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
@endsection
