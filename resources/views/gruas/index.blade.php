<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Listado de gruas</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
  <h1>Listado de gruas</h1>
  <table class="table table-stripped table-condensed table-hovered">
      <thead>
        <tr>
          <th>description</th>
          <th>capacity</th>
          <th>power_boom</th>
          <th>fixed_jib</th>
          <th>counterweight</th>
          <th>engine</th>
          <th>gross_weight</th>
          <th>tires</th>
          <th>transmission</th>
          </tr>
        </thead>
      <tbody>
          @foreach ($gruas as $grua)
            <tr>
            <td>{{ $grua->description }}</td>
            <td>{{ $grua->capacity }}</td>
            <td>{{ $grua->power_boom }}</td>
            <td>{{ $grua->fixed_jib }}</td>
            <td>{{ $grua->counterweight }}</td>
            <td>{{ $grua->engine }}</td>
            <td>{{ $grua->gross_weight }}</td>
            <td>{{ $grua->tires }}</td>
            <td>{{ $grua->transmission }}</td>
            </tr>
          @endforeach
      </tbody>
  </table>
      <div class="text-center">
          {{ $gruas->links() }}
      </div>
</body>
</html>
