$(document).ready(function (){
  $("#table_search").keyup(function(){
     $.ajax({
       url:'https://jsonplaceholder.typicode.com/users/'+$("#table_search").val(),
       method:'GET',
       beforSend: function (){
         $("service-data").html("BUSCANDO...");
       },
       success: function(data){
         var html;
         html+='<td>'+data.name+'</td><td>'+data.website+'</td>';
         html+='<td>'+data.username+'</td><td>'+data.company.name+'</td>';
         $("#service-data").html(html);
       }
     });
  });
});
