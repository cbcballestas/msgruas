@extends('templates.template')
@section('bienvenida')
  @include('partials.bienvenida')
@endsection
@section('content')
    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">
        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <h2 class="title">Noticias</h2>
            <a class="twitter-timeline" href="https://twitter.com/Machinery_Servi?ref_src=twsrc%5Etfw">Tweets by Machinery_Servi</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>

          <div class="col-lg-6 background order-lg-2 order-1 wow fadeInRight"></div>
        </div>

      </div>
    </section><!-- #about -->
</main>
@endsection
