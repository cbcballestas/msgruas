<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>@yield('title','MS Machinery & Service')</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{{ asset('img/favicon.png') }}" rel="icon">
  <link href="{{ asset('img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('lib/animate/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">

  <!-- =======================================================
    Theme Name: Regna
    Theme URL: https://bootstrapmade.com/regna-bootstrap-onepage-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" style="background-color: #f6b416">
    <div class="container">

      <div id="logo" class="pull-left">
        <a href="#hero"><img src="{{ asset('img/logo.png') }}" alt="" title="" /></img></a>
        <!-- Uncomment below if you prefer to use a text logo -->
        <!--<h1><a href="#hero">Regna</a></h1>-->
      </div>

      @include('partials.navbar')
    </div>
  </header><!-- #header -->

  @yield('bienvenida')

  <main id="main">
      @yield('content')
  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
      <div class="container wow fadeIn" style="margin-top: 50px">
        <div class="row">
          <div class="col-lg-4 col-md-6 wow fadeInUp">
            <div class="box">
              <div class="full_third" id="contacto" style="border-right: 1px solid #fff; margin-bottom: 40px">
              <b>Contactenos</b>
              <h5><i>Preciona a continuacion para conocer nuestra ubicacion</i></h5>
              <br>
              <a href="{{ url('contacto')}}"><img id="map" src="{{ asset('img/footer/map-hover2.png') }}" alt="nuestra ubicacion" style="width: 65%;height: 65%;"></a>
            </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 wow fadeInUp">
            <div class="box">
              <div class="full_third" id="folleto" style="border-right: 1px solid #fff;">
                <b>Descarga nuestro portaforio:</b>
                <div id="imgPortaforio" style = "float: left"></div>
                <div id="description" style = "float: left"></div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 wow fadeInUp" style="border-right: 1px solid #fff;">
            <div class="box">
              <div class="full_third" id="sociales">
                <b>Siguenos en:</b><br>
                  <i>Para tener toda la informacion actualizada de nuestras noticias, servicios y proyectos</i><br>
                    <div class="social-links">
                      <a href="{{ asset('https://twitter.com/Machinery_Servi') }}"><i class="fa fa-twitter-square" style="font-size:48px"></i></a>
                      <a href="{{ asset('https://www.facebook.com/msgruas/') }}" class="facebook"><i class="fa fa-facebook-square" style="font-size:48px"></i></a>
                      <a href="" class="instagram"><i class="fa fa-instagram" style="font-size:48px"></i></a>
                      <a href="#" class="linkedin"><i class="fa fa-youtube-play" style="font-size:48px"></i></a>
                </div><br>
            <img src="{{ asset('img/logo.png') }}">
        </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    <div class="container" style="margin-top: 90px">
      <div class="copyright">
        &copy; Copyright <strong>Msgruas</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Regna
        -->
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries
  <script src="//code.jquery.com/jquery-3.0.0.min.js"></script>-->
  <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('lib/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('lib/wow/wow.min.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>

  <script src="{{ asset('lib/waypoints/waypoints.min.js') }}"></script>
  <script src="{{ asset('lib/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('lib/superfish/hoverIntent.js') }}"></script>
  <script src="{{ asset('lib/superfish/superfish.min.js') }}"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
