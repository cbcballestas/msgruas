@extends('templates.template')
@section('title','MS Machinery & Service | Nosotros')
@section('content')
  <main id="main">
    <!--==========================
      Team nosotros
    ============================-->
 <div id="titulo" style="margin-top: 60px;"  class=" wow fadeInUp">
      <div style="background-image: url({{ asset('img/tittle.jpg') }}); height: 210px;"><br><br><br><br>
        <div class="text">
                <h1 style="color: orange; text-align: center"><b>Nuestra Empresa</b></h1>
            </div>
      </div>
    </div>

    <section id="about">
      <div class="container">
        <div class="row about-container">
          <div class="col-lg-6 content order-lg-1 order-2" style="margin-top: 20px;" >
            <h2 class="title" style="color: #f6b416">¿QUIENES SOMOS?</h2>
            <p style="text-align: justify;">
            	Somos una importante compañía a nivel nacional en materia de izamiento de carga y transporte especial. Nos dedicamos a la prestación de servicios y venta de maquinaria, repuestos y accesorios en diferentes sectores. <br><br>

			       	Además de lo antes expuesto tenemos un gran compromiso con la salud, seguridad y medio ambiente. Superar las expectativas de nuestros clientes, constituye la base de nuestro éxito. 
            </p>
          </div>
      <img src="{{ asset('img/carousel/4.jpg') }}" style="width: 500px;height: 350px">
      <div class="container wow fadeIn" style="margin-top: 50px">
        <div class="row">
         
          <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="box">
            	<hr>
              <h4 class="title" style="color: #f6b416">MISION</h4>
              <p class="description" style="text-align: justify;">Ser líderes en la prestación de servicio con equipos de izamiento de carga y transporte especial, satisfaciendo las necesidades de nuestros clientes por encima de sus expectativas, brindándoles servicios con equipos de calidad. Asimismo, MS GRUAS, tiene como objetivo el crecimiento profesional de sus colaboradores.</p>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="box">
            	<hr>
              <h4 class="title" style="color: #f6b416">VISION</h4>
              <p class="description">Ser una compañía reconocida en materia de izamiento de carga y transporte especial, por nuestra innovación, soluciones, equipos y servicios, con competencia nacional.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="box">
            	<hr>
              <h4 class="title" style="color: #f6b416">VALORES CORPORATIVOS</h4>
             <p class="description">	
      				<b>HONESTIDAD:</b> En todos Nuestros Actos<br>
      				<b>EFICIENCIA:</b> En nuestros procesos productivos y administrativos.<br>
      				<b>CALIDAD:</b> En nuestras obras y servicios.<br>
      				<b>SEGURIDAD:</b> Para nuestro personal y las instalaciones encomendadas</p>
            </div>
          </div>

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-top: 10px;width: 100%; height: 20%">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="img/carousel/5.jpg" alt="..." style="width: 100%px;height: 200PX;">
    </div>
    <div class="item">
      <img src="img/carousel/6.jpg" alt="..." style="width: 1366px;height: 200PX;">
    </div>
    <div class="item">
      <img src="img/carousel/7.jpg" alt="..." style="width: 1366px;height: 200PX;">
    </div>
    <div class="item">
      <img src="img/carousel/8.jpg" alt="..." style="width: 1366px;height: 200PX;">
    </div>
    <div class="item">
      <img src="img/carousel/9.jpg" alt="..." style="width: 1366px;height: 200PX;">
    </div>
  </div>
</div>

      </div>
      	</div>
    </section><!-- #about -->
  </main>
  @endsection