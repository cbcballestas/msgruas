@extends('admin.template.template')
@section('title','Servicios - Nuevo Servicio')
@section('page_name','Servicios')
@section('small','Nuevo Servicio')
@section('breadcrumb')
  <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
  <li class="active">Crear Servicio</li>
@endsection
@section('content')
  <div class="box box-primary">
    <div class="box-header with-border">
      <h2 class="box-title">AGREGAR SERVICIO</h2>
    </div>
  {!! Form::open(['method' => 'POST', 'route' => 'servicios.store','role'=>'form','files'=>true,'autocomplete'=>'off']) !!}
  <div class="box-body">
    <div class="row">
    <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }} col-sm-6">
        {!! Form::label('titulo', 'Nombre del servicio') !!}
        {!! Form::text('titulo', null, ['class' => 'form-control', 'required' => 'required','autofocus']) !!}
        <small class="text-danger">{{ $errors->first('titulo') }}</small>
    </div>
  </div>
    <div class="row">
    <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }} col-sm-6">
        {!! Form::label('descripcion', 'Descripción del servicio') !!}
        {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('descripcion') }}</small>
    </div>
    </div>
    <div class="form-group{{ $errors->has('profile_photo') ? ' has-error' : '' }}">
            {!! Form::label('profile_photo', 'Portada de servicio') !!}
            {!! Form::file('profile_photo', ['required' => 'required']) !!}
            <p class="help-block">Seleccione imagen de perfil</p>
            <small class="text-danger">{{ $errors->first('profile_photo') }}</small>
    </div>
    <div class="form-group{{ $errors->has('photos') ? ' has-error' : '' }}">
            {!! Form::label('photos', 'Fotos para galería') !!}
            {!! Form::file('photos[]', array('multiple' => true)) !!}
            <p class="help-block">Seleccione fotos para galería</p>
            <small class="text-danger">{{ $errors->first('photos') }}</small>
    </div>
  <div class="btn-group pull-right" style="margin-right:20px">
      <div class="row">
      {!! Form::submit("Agregar", ['class' => 'btn btn-success btn-flat btn-lg']) !!}
      </div>
  </div>
  {!! Form::close() !!}
</div>
@endsection
