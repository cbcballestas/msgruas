<?php

use Illuminate\Database\Seeder;
use App\Grua;

class GruasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(Grua::class,10)->create();
    }
}
