<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio;

class ServiciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $servicios = Servicio::where('estado', '=', 1)->search($request->servicio_search)->orderBy('created_at')->paginate(10);

        return view('admin.servicios.index', ['servicios' => $servicios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.servicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $servicio = new Servicio();
        // getting all of the post data
        $galeria = $request->file('photos');
        $portada = $request->file('profile_photo');

        if ($request->hasFile('photos') && $request->hasFile('profile_photo')) {
            // cambiandole nombre a la portada
            $portada_name = time().'_'.$portada->getClientOriginalName();
            $servicio->foto = $portada_name;
            $servicio->titulo = $request->titulo;
            $servicio->descripcion = $request->descripcion;
            $servicio->slug = str_slug($servicio->titulo, '-');
            $servicio->save();

            // guardando portada
            \Storage::disk('portadas')->put($portada_name, \File::get($portada));

            // guardando galeria
            foreach ($galeria as $file) {
                $filename = time().'_'.$file->getClientOriginalName();
                \Storage::disk('galerias')->put(str_slug($request->titulo, '-').'/'.$filename, \File::get($file));
                $servicio->galerias()->create(['img_name' => $filename, 'servicio_id' => $servicio]);
            }

            \Session::flash('success_message', "El servicio ".$servicio->titulo." ha sido creado con éxito!!");

            return redirect()->route('servicios.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = Servicio::where('slug', '=', $id)->first();
        $servicio->each(function ($servicio) {
            $servicio->galerias;
        });

        return view('admin.servicios.edit', compact('servicio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $galeria = $request->file('galerias');
        $portada = $request->file('profile_photo');
        $servicio = Servicio::where('slug', '=', $id)->first();
        if ($request->hasFile('profile_photo')) {
            // cambiandole nombre a la portada
            $portada_name = time().'_'.$portada->getClientOriginalName();
            \Storage::disk('portadas')->delete($servicio->foto);
            \Storage::disk('portadas')->put($portada_name, \File::get($portada));
            $servicio->foto = $portada_name;
        }

        if ($request->hasFile('galerias')) {
            // guardando galeria
            foreach ($galeria as $file) {
                $filename = time().'_'.$file->getClientOriginalName();
                \Storage::disk('galerias')->put(str_slug($request->titulo, '-').'/'.$filename, \File::get($file));
                $servicio->galerias()->create(['img_name' => $filename, 'servicio_id' => $servicio]);
            }
        }

        $servicio->titulo = $request->titulo;
        $servicio->descripcion = $request->descripcion;
        $servicio->slug = str_slug($servicio->titulo, '-');
        $servicio->save();

        \Session::flash('success_message', "El servicio ".$servicio->titulo." ha sido actualizado con éxito!!");

        return redirect()->route('servicios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $s = Servicio::where('slug', '=', $id)->first();
        $s->estado = 0;
        $s->save();
        \Session::flash('delete_message', "El servicio ".$s->titulo." ha sido eliminado con éxito!!");

        return redirect()->route('servicios.index');
    }

    public function getServicio($nombre)
    {
        $servicio = Servicio::where('slug', '=', $nombre)->first();
        $servicio->each(function ($servicio) {
            $servicio->galerias;
        });

        return view('servicios', compact('servicio'));
    }
}
