/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  'use strict';
  /* Morris.js Charts */
  // Sales chart
  var area = new Morris.Bar({
    element   : 'revenue-chart',
    resize    : true,
    data      : [
      { y: '2016', item1: 2666 },
      { y: '2016', item1: 2778 },
      { y: '2016', item1: 4912 },
      { y: '2016', item1: 3767 },
      { y: '2017', item1: 6810 },
      { y: '2017', item1: 5670 },
      { y: '2017', item1: 4820 },
      { y: '2017', item1: 15073 },
      { y: '2018', item1: 10687 },
      { y: '2018', item1: 8432 }
    ],
    xkey      : 'y',
    ykeys     : ['item1'],
    labels    : ['Item 1'],
    lineColors: ['#3c8dbc'],
    hideHover : 'auto'
  });

  // Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    area.redraw();
  });
});
